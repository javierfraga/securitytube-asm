#my first assembly program

.data

HelloWorldString:
    .ascii "Hello World\n"
HelloWorldString2:
    .ascii "some other text\n"

.text

.globl _start

_start:
    # load all args for write()
    movl    $4 ,    %eax
    movl    $1 ,    %ebx
    movl    $HelloWorldString , %ecx
    movl    $12 ,   %edx
    int     $0x80

    # need to exit the program
    movl    $1 , %eax
    movl    $0 , %ebx
    int     $0x80
