#Demo program to show how to use Data types and MOVx instructions

.data
    HelloWorld:
        .ascii "HelloWorld!\n"
    ByteLocation:
        .byte 10
    Int32:
        .int 2 # size word
    Int16:
        .short 3 # size halfword
    Float:
        .float 10.23
    IntegerArray:
        .int 10 , 20 , 30 , 40 , 50

.bss
    .comm LargeBuffer , 10000

.text
    .globl _start

    _start:
        nop
        # 1. MOVE immediate value into register
        movl    $10 ,    %eax
        # 2. MOV iimmediate value into memory locations
        movw    $50 ,    Int16
        # 3. MOV data betwen registers
        movl    %eax ,  %ebx
        # 4. MOV data from memory to registers
        movl    Int32 , %eax
        # 5. MOV data from register to memory
        movb    $3 ,    %al
        movb    %al ,   ByteLocation
        # 6. MOV data into an indexed memory location
        # location is decided by BaseAddress( Offset , Index , DataSize)
        # offset and Index must be registers , Datasize can be numerical value
        movl    $0 ,    %ecx
        movl    $2 ,    %edi
        movl    $22,    IntegerArray( %ecx , %edi , 4 )
        # 7. Indirect addressing using registers
        movl    $Int32 ,    %eax
        movl    (%eax) ,    %ebx
        movl    $9 , (%eax)
        #Exit syscall to exit the program
        movl    $1 ,    %eax
        movl    $0 ,    %ebx
        int     $0x80

